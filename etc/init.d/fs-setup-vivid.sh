#!/bin/sh

do_check_ext4()
{
	blkid $1 | grep "TYPE=\"ext4\""
	if [ $? -ne 0 ]; then
		echo "formating " $1 "to ext4..."
		mkfs.ext4 -O ^metadata_csum -L "$2" $1
	else
		echo $1 "already format by ext4"
	fi
}

do_check_format()
{
	local device=$1
	local label=$2
	if [ -h /dev/disk/by-label/$label ]; then
		return
	fi

	#emmc
	[ ${device:0:6} = "mmcblk" ] && {
		do_check_ext4 /dev/$device $label
		return
	}
	#nand
	[ ${device:0:4} = "nand" ] && {
		do_check_ext4 /dev/$device $label
		return
	}
	#nor
	#[ ${lnk:0:8} = "mtdblock" ] && {
	#	do_check_jffs2 $1 $2
	#	return
	#}
}

do_format_by_label()
{
	target=$1

	if [ -h /dev/disk/by-label/UDISK ]; then
		exit
	fi

	for line in `cat /proc/cmdline`
	do
		if [ ${line%%=*} = 'partitions' ] ; then
			partitions=${line##*=}
			part=" "
			while [ "$part" != "$partitions" ]
			do
				part=${partitions%%:*}
				device=${part#*@}
				label=${part%@*}
				if [ ${label} = "${target}" ]; then
					do_check_format ${device} ${label}
					return
				fi
				partitions=${partitions#*:}
			done
		fi
	done
}

do_mount_path()
{
	mount_path=$1

	mountpoint -q $mount_path
	if [ $? -ne 0 ]; then
		mount $mount_path
	fi
}

if [ -d /mnt/UDISK ]; then
	do_format_by_label UDISK
	do_mount_path /mnt/UDISK
fi

if [ -d /usr/games/save ]; then
	do_format_by_label rootfs_data
	do_mount_path /usr/games/save
fi

