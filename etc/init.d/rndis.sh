#!/bin/sh

echo "rndis" > /sys/class/android_usb/android0/functions
echo "1" > /sys/class/android_usb/android0/f_rndis/wceis
echo "1" > /sys/class/android_usb/android0/enable
ifconfig rndis0 169.254.215.100 netmask 255.255.0.0
